# -*- coding: utf-8 -*-
"""
Created on Tue Feb 18 20:21:04 2020

@author: saipr
"""
import requests, json
import requests.auth
import pandas as pd
import time
import datetime
import numpy as np
import pytz

tz = pytz.timezone('Asia/Calcutta')
mailpath='/home/admin/CODE/Send_mail/mail_recipients.csv'

def rectolist(station,path):
    df=pd.read_csv(path)
    df2=df[['Recipients',station]]
    a=df2.loc[df2[station] == 1]['Recipients'].tolist()
    return a

def sendmail(data,rec,cov):
    SERVER = "smtp.office365.com"
    FROM = 'operations@cleantechsolar.com'
    recipients = rec # must be a list
    TO=", ".join(recipients)
    SUBJECT = 'IN-050 CoV Alarm'
    text2='\n---------------------------------\nCOV\n---------------------------------\n\nCOV Yields: '+str(round(cov,1))+'%\n\n---------------------------------\nInduvidual Meter Yields\n---------------------------------\n\n'
    for i in sorted(data):
        text2=text2+'Yield '+str(i)+' [kWh/kWp]: '+str(data[i])+'\n\n'
    TEXT = text2
    # Prepare actual message
    message = "From:"+FROM+"\nTo:"+TO+"\nSubject:"+ SUBJECT +"\n\n"+TEXT
    # Send the mail
    import smtplib
    server = smtplib.SMTP(SERVER)
    server.starttls()
    server.login('shravan.karthik@cleantechsolar.com', 'CTS&*(789')
    server.sendmail(FROM, recipients, message)
    server.quit()
    
def getyields(date):
    url={
    'TP1-MFM1':['http://fbapi.fleximc.com/API/JSON/CleanTech/Arvind_Mills/Data/'+date+'/32_MFM_6_'+date+'.json',292.5],
    'TP2-MFM1':['http://fbapi.fleximc.com/API/JSON/CleanTech/Arvind_Mills/Data/'+date+'/32_MFM_7_'+date+'.json',282.1],
    'Achal-MFM1':['http://fbapi.fleximc.com/API/JSON/CleanTech/Arvind_Mills/Data/'+date+'/32_MFM_4_'+date+'.json',806.4],
    'Achal-MFM2':['http://fbapi.fleximc.com/API/JSON/CleanTech/Arvind_Mills/Data/'+date+'/32_MFM_5_'+date+'.json',998.4],
    'Anklesh-MFM1':['http://fbapi.fleximc.com/API/JSON/CleanTech/Arvind_Mills/Data/'+date+'/32_MFM_1_'+date+'.json',1003.2],
    'Anmol-MFM1':['http://fbapi.fleximc.com/API/JSON/CleanTech/Arvind_Mills/Data/'+date+'/32_MFM_2_'+date+'.json',780.8],
    'Anmol-MFM2':['http://fbapi.fleximc.com/API/JSON/CleanTech/Arvind_Mills/Data/'+date+'/32_MFM_3_'+date+'.json',1024],
    'Classic 1-MFM1':['http://13.127.216.107/API/JSON/swl/ArvindMillsClassic1/Data/'+date+'/33_MFM_1_'+date+'.json',455],
    'Classic 1-MFM2':['http://13.127.216.107/API/JSON/swl/ArvindMillsClassic1/Data/'+date+'/33_MFM_2_'+date+'.json',312],
    'Yarn Godown-MFM1':['http://13.127.216.107/API/JSON/swl/ArvindMillsYarnGodown/Data/'+date+'/34_MFM_1_'+date+'.json',448.5],
    'SSR-MFM1':['http://13.127.216.107/API/JSON/swl/ArvindMillsSSR/Data/'+date+'/37_MFM_1_'+date+'.json',513.5],
    'SSR-MFM2':['http://13.127.216.107/API/JSON/swl/ArvindMillsSSR/Data/'+date+'/37_MFM_2_'+date+'.json',1105],
    'SSR-MFM3':['http://13.127.216.107/API/JSON/swl/ArvindMillsSSR/Data/'+date+'/37_MFM_3_'+date+'.json',598],
    'X-MFM1':['http://13.127.216.107/API/JSON/swl/ArvindMillsClassic234/Data/'+date+'/39_MFM_1_'+date+'.json',132],
    'C234-MFM1':['http://13.127.216.107/API/JSON/swl/ArvindMillsClassic234/Data/'+date+'/39_MFM_2_'+date+'.json',633.6],
    'C234-MFM2':['http://13.127.216.107/API/JSON/swl/ArvindMillsClassic234/Data/'+date+'/39_MFM_3_'+date+'.json',672],
    'Bottom-MFM1':['http://13.127.216.107/API/JSON/swl/Bottom/Data/'+date+'/43_MFM_1_'+date+'.json',454.4],
    'Bottom-MFM2':['http://13.127.216.107/API/JSON/swl/Bottom/Data/'+date+'/43_MFM_2_'+date+'.json',245.2],
    'Bottom-MFM4':['http://13.127.216.107/API/JSON/swl/Bottom/Data/'+date+'/43_MFM_4_'+date+'.json',332.8],
    'Bottom-MFM3':['http://13.127.216.107/API/JSON/swl/Bottom/Data/'+date+'/43_MFM_3_'+date+'.json',345.2],
    'Denim & Shed 100-MFM1':['http://fbapi.fleximc.com/API/JSON/CleanTech/Denim_Shed_100/Data/'+date+'/44_MFM_1_'+date+'.json',889.6],
    'AMD-MFM1':['http://13.127.216.107/API/JSON/swl/ArvindMillsAMD/Data/'+date+'/45_MFM_1_'+date+'.json',1256],
    'AMD-MFM2':['http://13.127.216.107/API/JSON/swl/ArvindMillsAMD/Data/'+date+'/45_MFM_2_'+date+'.json',754],
    'Yarn Warehouse-MFM1':['http://fbapi.fleximc.com/API/JSON/CleanTech/Yarn_Warehouse/Data/'+date+'/47_MFM_1_'+date+'.json',299],
    'Sample Library-MFM1':['http://13.127.216.107/API/JSON/swl/SampleLibrary/Data/'+date+'/48_MFM_1_'+date+'.json',172.8],
    'Amber Warehouse-MFM1':['http://13.127.216.107/API/JSON/swl/Amber/Data/'+date+'/49_MFM_1_'+date+'.json',156]
    }
    return url

print('Started!')
while(1):
    if(datetime.datetime.now(tz).hour==0):
        date=datetime.datetime.now(tz)+datetime.timedelta(days=-1)
        date=date.strftime('%Y%m%d') 
        url=getyields(date)
        ylds=dict()
        for i in url:
            print(i)
            while True:
                try:
                    r = requests.get(url[i][0])
                    data4=r.json()
                    dataframe=pd.DataFrame(data4['MFM'])
                    dataframe.rename(columns= {
                    'm8':'Voltage_R_Phase','m9':'Voltage_Y_Phase','m10':'Voltage_B_Phase','m11':'Average_Voltage','m12':'Voltage_R_Y','m13':'Voltage_Y_B','m14':'Voltage_B_R',
                    'm15':'Line_To_Line_Voltage_Average','m16':'Current_R','m17':'Current_Y','m18':'Current_B','m19':'Current_N','m20':'Average_Current','m21':'Frequency_R',
                    'm22':'Frequency_Y','m23':'Frequency_B','m24':'Average_Frequency','m25':'Power_Factor_R','m26':'Power_Factor_Y','m27':'Power_Factor_B','m28':'Average_Power_Factor',
                    'm29':'Active_Power_R','m30':'Active_Power_Y','m31':'Active_Power_B','m32':'Total_Power','m33':'Reactive_Power_R','m34':'Reactive_Power_Y','m35':'Reactive_Power_B',
                    'm36':'Total_Reactive_Power','m37':'Total_Apparent_Power','m38':'Active_Energy','m39':'Reactive_Energy','m40':'Apparent_Power_R','m41':'Apparent_Power_Y',
                    'm42':'Apparent_Power_B','m43':'Reactive_Energy2','m44':'Maximum_Active_Power','m45':'Minimum_Active_Power','m46':'Maximum_Reactive_Power','m47':'Minimum_Reactive_Power',
                    'm48':'Maximum_Apparent_Power','m49':'Wh_Received','m50':'VAh_Received','m51':'VARh_Inductive_Received','m52':'VARh_Capacitive_Received','m53':'Energy_Export1',
                    'm54':'VAh_Delivered','m55':'VARh_Ind_Delivered','m56':'VARh_Cap_Delivered','m57':'THD','m58':'Active_Total_Import','m59':'Active_Total_Export',
                    'm60':'Apparent_Import','m61':'Aparent_Export','m62':'Energy_Today','m63':'Tstamp','m64':'Energy_Import','m65':'Energy_Export','m67':'Total_KW_Avg'
                    }, inplace=True)
                except:
                    time.sleep(5)
                    continue
                break 
            capacity=url[i][1]
            diff=dataframe['Energy_Export'].iloc[-1]-dataframe['Energy_Export'].iloc[0]
            if(i=='Sample Library-MFM1' or i=='Amber Warehouse-MFM1'):
                ylds[i]=round(diff/(capacity),2)
                print(round(diff/(capacity),2))
                print(ylds.values())
            else:
                ylds[i]=round(diff/(capacity*1000),2)
                print(round(diff/(capacity*1000),2))
                print(ylds.values())
        yld_vals=list(ylds.values())
        cov=round(np.std(yld_vals)*100/np.mean(yld_vals),1)
        print(cov)
        if(cov>5):
            recipients=rectolist('IN-050C_Mail',mailpath)
            sendmail(ylds,recipients,cov)
    else:
        print('Sleeping!')
    time.sleep(3600)

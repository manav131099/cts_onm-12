from ftplib import FTP
import pandas as pd
import time
import datetime
import pytz
import io
import urllib.request
import re 
import os
import numpy as np

pd.options.mode.chained_assignment = None



def chkdir(path):
    a=os.path.isdir(path)
    if(a):
        return
    else:
        os.makedirs(path)
        
path='/home/admin/Dropbox/SERIS_Live_Data/[IN-015S]/'
chkdir(path)
tz = pytz.timezone('Asia/Singapore')
curr=datetime.datetime.now(tz)
currnextmin=curr + datetime.timedelta(minutes=1)
currnextmin=currnextmin.replace(second=9,microsecond=500000)
while(curr!=currnextmin):
    curr=datetime.datetime.now(tz)
    curr=curr.replace(microsecond=500000)
    time.sleep(1)
currtime=curr.replace(tzinfo=None)
print("Start time is",curr)
cols=[]
for i in range(600):
    cols.append(i+1)
print("Starting Live Bot!")
ftp = FTP('ftpnew.cleantechsolar.com')
ftp.login(user='cleantechsolar', passwd = 'bscxataB2')
ftp.cwd('1min')
noneflag=1
flag=1
starttime=time.time()
cols2=['Time Stamp','AvgGsi00','AvgGmod10N','AvgGmod10S','AvgGmod10E','AvgGmod10W','AvgHamb','AvgTamb','AvgTmodN','AvgTmodS','Act_Pwr-Tot_LV','Act_Pwr-Tot_HV','Act_E-Del-Rec_LV','Act_E-Del-Rec_HV']
while(1):
    if(flag==1):
        cur=currtime.strftime("%Y-%m-%d_%H-%M")
        cur2=currtime+datetime.timedelta(hours = -2,minutes=-30)
        currtime2=cur2.replace(tzinfo=None)
        prev=currtime2+datetime.timedelta(days=-1)
        prev=str(prev)
        cur3=currtime2.strftime("%Y-%m-%d_%H-%M")
        print('Now adding for',currtime2)
    else:
        curr=datetime.datetime.now(tz)
        cur2=curr+datetime.timedelta(hours = -2,minutes=-30)
        currtime2=cur2.replace(tzinfo=None)
        prev=currtime2+datetime.timedelta(days=-1)
        prev=str(prev)
        cur3=currtime2.strftime("%Y-%m-%d_%H-%M")
        currtime=curr.replace(tzinfo=None)
        cur=currtime.strftime("%Y-%m-%d_%H-%M")
        print('Now adding for',currtime)
    flag2=1
    j=0
    while(j<5 and flag2!=0):
        j=j+1
        files=ftp.nlst()
        for i in files:
            if(re.search(cur,i)):
                req = urllib.request.Request('ftp://cleantechsolar:bscxataB2@ftpnew.cleantechsolar.com/1min/Cleantech-'+cur+'.xls')
                try:
                    with urllib.request.urlopen(req) as response:
                        s = response.read()
                    df=pd.read_csv(io.StringIO(s.decode('utf-8')),sep='\t',names=cols)
                    a=df.loc[df[1] == 712] 
                    if(os.path.exists(path+cur3[0:4]+'/'+cur3[0:7]+'/'+'[IN-015S] '+cur3[0:10]+'.txt') and noneflag==0):
                        b=a[[2,3,4,5,6,7,8,9,10,11,25,72,43,88]] 
                        b.loc[:,[25,72,43]]=b.loc[:,[25,72,43]]*-1
                        print("B is",b)  
                        df= pd.read_csv(path+cur3[0:4]+'/'+cur3[0:7]+'/'+'[IN-015S] '+cur3[0:10]+'.txt',sep='\t')
                        df2= pd.DataFrame( np.concatenate( (df.values, b.values), axis=0 ) )
                        df2.columns = cols2
                        print(df2)
                        std1=df2['Act_E-Del-Rec_LV'].std()
                        std2=df2['Act_E-Del-Rec_HV'].std()
                        print(std1)
                        print(std2)
                        if(std1>13000):
                            b.loc[:, 43] = 'NaN' 
                        if(std2>13000):
                            b.loc[:, 88] = 'NaN' 
                        b.loc[b[43] == 0, 43] = 'NaN'
                        b.loc[b[88] == 0, 88] = 'NaN'
                    else:
                        print("In else")
                        noneflag=1
                        b=a[[2,3,4,5,6,7,8,9,10,11,25,72,43,88]] 
                        b.loc[:,[25,72,43]]=b.loc[:,[25,72,43]]*-1
                        print(b,prev)
                        dfprev= pd.read_csv(path+prev[0:4]+'/'+prev[0:7]+'/'+'[IN-015S] '+prev[0:10]+'.txt',sep='\t')
                        dfprev2= pd.DataFrame( np.concatenate( (dfprev.values, b.values), axis=0 ) )
                        dfprev2.columns = cols2
                        print(dfprev2.tail())
                        std1=dfprev2['Act_E-Del-Rec_LV'].std()
                        std2=dfprev2['Act_E-Del-Rec_HV'].std()
                        if(std1>10000):
                            b.loc[:, 43] = 'NaN' 
                        if(std2>10000):
                            b.loc[:, 88] = 'NaN' 
                        b.loc[b[43] == 0,43] = 'NaN'
                        b.loc[b[88] == 0, 88] = 'NaN'
                        print(std1)
                        print(std2)
                        if(b[43].isnull().values.any() or b[88].isnull().values.any() or std1>10000 or std2>10000 or std1==np.nan or std2==np.nan):
                            pass
                        else:
                            noneflag=0
                        flag=0

                    #Taking care of Power
                    b.loc[(b[3] < 0), 3] = 0
                    b.loc[(b[25] > 3000) | (b[25] < 0), 25] = 'NaN'
                    b.loc[(b[72] > 3000) | (b[72] < 0), 72] = 'NaN'
                    b.columns=cols2
                    chkdir(path+cur3[0:4]+'/'+cur3[0:7])
                    if(os.path.exists(path+cur3[0:4]+'/'+cur3[0:7]+'/'+'[IN-015S] '+cur3[0:10]+'.txt')):
                        b.to_csv(path+cur3[0:4]+'/'+cur3[0:7]+'/'+'[IN-015S] '+cur3[0:10]+'.txt',header=False,sep='\t',index=False,mode='a')
                    else:
                        b.to_csv(path+cur3[0:4]+'/'+cur3[0:7]+'/'+'[IN-015S] '+cur3[0:10]+'.txt',header=True,sep='\t',index=False)
                    flag2=0
                except Exception as e:
                    flag2=0
                    print(e)
                    print("Failed for",currtime2)
        if(flag2!=0 and j<5):
            print('sleeping for 10 seconds!')            
            time.sleep(10)
    if(flag2==1):
        print('File not there!',cur)
    print('sleeping for a minute!')
    time.sleep(60.0 - ((time.time() - starttime) % 60.0))
    
        
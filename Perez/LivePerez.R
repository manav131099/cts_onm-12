rm(list = ls())

require('insol')
errHandle = file('/home/admin/Logs/LogsPerez.txt',open='w',encoding='UTF-8')
sink(errHandle,type='message',append = T)
sink(errHandle,type='output',append = T)
source('/home/admin/CODE/Perez/SolMod.R')

d2r <- function(theta)
{
  return (theta * .0174532925)
}

calZen <- function(Tm, lat, lon, tz, tilt, orientation)
{
  #tilt angle between 0 and 90
  #orientation between 0 and 360, north 0, east positive(clockwise)
  jd = JD(Tm)
  sunv = sunvector(jd, lat, lon, tz)
  azi = round(sunpos(sunv)[,1],3)#azimuth of the sun
  zen = round(sunpos(sunv)[,2],3)#zenith angle
  zen = zen
  surface.norm = normalvector(tilt, orientation)
  inc = round(as.numeric(degrees(acos(sunv%*% as.vector(surface.norm)))),3)
  dec = declination(jd)*pi/180
  re = 1.000110+0.034221*cos(dec)+0.001280*sin(dec)+0.00719*cos(2*dec)+0.000077*sin(2*dec)
  Io = round(1362*re,3)#extraterrestrial direct normal irradiance
  Ioh = round(1362*re*cos(zen*pi/180))#horizontal extraterrestrial irradiance
  Ic = round(0.8298*1362*re*(cos(d2r(zen)))^1.3585*exp(-0.00135*(90-zen)), 1)
  Ioh = ifelse(zen>=90, 0, Ioh)
  Ic = ifelse(zen>=90, 0, Ic)
  out = list(Tm,zen,azi,inc, Io, Ioh, Ic)
  names(out) = c("Time","Zenith","Azimuth","Incidence", "Io", "Ioh","Ic")
  out
}

checkdir = function(x)
{
  if(!file.exists(x))
    dir.create(x)
}

resetList = function()
{
  TMData <<- c()
  GHIData <<- c()
  DHIData <<- c()
  TambData <<- c()
  TiltData <<- c()
  AziData <<- c()
  GmodData <<- c()
  ZenData <<- c()
  Incidence <<- c()
  skyBrightness <<- c()
	latData <<- c()
	lonData <<- c()
	solarElevation <<- c()
}

threshAdjust = 0.5
threshAdjustCity = 0
defaultTilt = 10
defaultAzi = 90

pathWrite = "/home/admin/Dropbox/GIS/Gen-1"
checkdir(pathWrite)

dayspermonth = c(31,28,31,30,31,30,31,31,30,31,30,31)
path = "/home/admin/Dropbox/GIS/Raw Files"
days = dir(path)
vals = days
countries = c("India","Cambodia","Phillipines","Singapore","Slovakia")
tzuse = c("Asia/Calcutta","Asia/Jakarta","Singapore","Singapore","Europe/Rome")
tzpass = c(6,7,8,8,2)
x=4

outer = 1
writeFile = 0
duplicateCheck = 1
DSTOffset = 0
TMData = c()
GHIData = c()
DHIData = c()
GTIData = c()
TambData = c()
TiltData = c()
AziData = c()
GmodData = c()
ZenData = c()
Incidence = c()
skyBrightness = c()
latData = lonData =  solarElevation = c()

DayError = c()
TmError = c()
FileInfoError = c()
ZenError = c()
SEError = c()
DiffError = c()
GlobalErrorCount = 1
adjustmentFactor = 1
#calculatePerez = function(vals)
#{
for(outer in 1 : length(vals))
{
  dataread = try(read.table(paste(path,vals[outer],sep="/"),header = T,sep=";"),silent=T)
  if(class(dataread)=='try-error')
    next
  idxSE = match("SE",colnames(dataread))
  if(!is.finite(idxSE))
	{
		print(paste('reading ',path,"/",vals[outer],sep=""))
		print('colnames dataread')
		print(colnames(dataread))
		print('----------------------------------------------------------')
	}
	country = as.character(unlist(strsplit(vals[outer],"_"))[6])
  
  idxDHI = 4
  idxTamb = 9
  idxGTI = 5
	adjustmentFactor = 1
  if(country=='Slovakia')
  {
    idxDHI = 5
    idxGTI = 4
    idxTamb = 6
		adjustmentFactor = 0.95
  }
  
  city = as.character(unlist(strsplit(vals[outer],"_"))[5])
  idxmatch = match(country,countries)
  splitvals = unlist(strsplit(vals[outer],"_"))
  tiltUse = defaultTilt
	if(city == 'Aurangabad' || city == 'Coimbatore' || city == 'Nasik')
		tiltUse = tiltUse * -1
	threshAdjustCity = 0
	if(city == 'Lalru' || city == 'Lilora' || city == 'Chennai')
		threshAdjustCity = 0.5
  aziUse = defaultAzi
  globCounter=1
  {
    if(length(splitvals) > 9)
    {
      tiltUse = as.numeric(unlist(strsplit(splitvals[7],"t"))[1])
      aziUse = as.numeric(unlist(strsplit(splitvals[8],"a"))[1])
    }
    else if(length(splitvals)==9)
    {
      aziUse = as.numeric(unlist(strsplit(splitvals[7],"a"))[1])
    }
  }
  dataread2 = readLines(paste(path,vals[outer],sep="/"))
  lat = as.character(dataread2[7])
  lon = as.character(dataread2[8])
  lat = as.numeric(unlist(strsplit(lat,":"))[2])
  lon = as.numeric(unlist(strsplit(lon,":"))[2])
  offset = tzpass[idxmatch]
  pathWriteStn = paste(pathWrite,city,sep="/")
  checkdir(pathWriteStn)
  idx0s = match(unique(as.character(dataread[,1])),as.character(dataread[,1]))
	print(idx0s)
  nextDayCount = 1
  x = 0
	print(paste('nrow dataread',nrow(dataread)))
  while(T)
  {
    x = x + 1
		if(x > nrow(dataread) || nextDayCount > length(idx0s))
      break
    print(x)
    if(x == idx0s[nextDayCount])
    {
      yr = unlist(strsplit(as.character(dataread[x,1]),"\\."))
      mon = paste(yr[3],yr[2],sep="-")
			DSTOffset=0
			if(as.numeric(yr[2]) < 4 || as.numeric(yr[2]) > 9)
				DSTOffset = 0
			if(city=='Bratislava')
			{
				aziUse = 180
				tiltUse = 36
			}
			if(city == 'Aurangabad' || city == "NewDelhi")
			{
				aziUse = 270
				tiltUse = 5
			}
			if(city == 'Coimbatore')
			{
				aziUse = 180
				tiltUse = 12
			}
			if(city == 'Nasik')
			{
				aziUse = 180
				tiltUse = 17
			}
			offset = tzpass[idxmatch] + DSTOffset
			print(paste('offset value is',offset))
      day = paste(yr[3],yr[2],yr[1],sep="-")
      filename = toupper(substr(city,1,3))
      filename = paste("[",filename,"-",tiltUse,"t-",aziUse,"az] ",day,".txt",sep="")
			filenameError = paste("[",filename,"-",tiltUse,"t-",aziUse,"az] ",day,sep="")
      yr = yr[3]
      pathWriteYr = paste(pathWriteStn,yr,sep="/")
      checkdir(pathWriteYr)
      pathWriteMon = paste(pathWriteYr,mon,sep="/")
      checkdir(pathWriteMon)
      pathWriteTilt = paste(pathWriteMon,"/",tiltUse,"t_",aziUse,"az",sep="")
      checkdir(pathWriteTilt)
      pathWriteFinal = paste(pathWriteTilt,filename,sep="/")
      if(file.exists(pathWriteFinal) && duplicateCheck)
      {
				print(paste('Skipping',pathWriteFinal))
        break
      }
    }
    writeFile=0
    hr = as.character(dataread[x,2])
    tot = unlist(strsplit(hr,":"))
    offset = 0 ############
    hr = (as.numeric(tot[1])+offset)%%24
    if(hr < 10)
      hr=paste("0",hr,sep="")
    tmval = paste(hr,tot[2],sep=":")
    tmpass = paste(as.character(dataread[x,1]),paste(tmval,"00",sep=":"))
    tmpass= as.POSIXct(tmpass,format="%d.%m.%Y %H:%M:%S",tz="GMT")
    if(is.na(tmpass))
      next
    tmpass = format(tmpass,tz=tzuse[idxmatch])
    if(is.na(tmpass))
      next
    gethr = unlist(strsplit(as.character(tmpass)," "))[2]
    hr = unlist(strsplit(as.character(gethr),":"))[1]
    hr = as.numeric(hr)
    if(is.na(hr))
      next
    tmpass = paste(as.character(dataread[x,1]),gethr)
    tmpass= as.POSIXct(tmpass,format="%d.%m.%Y %H:%M:%S",tz=tzuse[idxmatch])
    if(hr == (tzpass[idxmatch]-1))
    {
      print('hr')
			print(hr)
			print('offset reached')
      writeFile = 1
      nextDayCount = nextDayCount + 1
    }
    globCounter = 1 + hr
    tmpass= as.POSIXct(tmpass,format="%d.%m.%Y %H:%M:%S",tz=tzuse[idxmatch])
    tm = as.POSIXct(tmpass,tz = (tzuse[idxmatch]))
    data = data.frame(calZen(tm,lat = lat,lon = lon,tz = 0,tilt = tiltUse,orientation = aziUse))
    daycountfinal=0
    daycount = as.numeric(substr(as.character(tm),6,7))
    if(daycount > 1)
      daycountfinal = sum(dayspermonth[1:(daycount-1)])
    daycountfinal = daycountfinal + as.numeric(substr(as.character(tm),9,10)) - 1
    zenvalue = round(as.numeric(data$Zenith),2)
    if(zenvalue > (90-threshAdjust-threshAdjustCity) && zenvalue < (90+threshAdjust+threshAdjustCity))
    {
      if(zenvalue <= 90)
        zenvalue = 90-threshAdjust-threshAdjustCity
      else
        zenvalue = 90+threshAdjust-threshAdjustCity
    }
  
    mr = 1/cos(d2r(zenvalue))
    tow = 2*pi*daycountfinal/sum(dayspermonth)
    I0 = 1362*(1+(0.033*cos(tow)))
    DHI = as.numeric(dataread[x,idxDHI])  
    GHI = as.numeric(dataread[x,3])
    GTI = as.numeric(dataread[x,idxGTI])
    TEMP = as.numeric(dataread[x,idxTamb])
    seuse = NA
    if(is.finite(idxSE))
      seuse = as.numeric(dataread[x,idxSE])
    skyBright = DHI * mr/I0
    dataframpass = data.frame(s = tiltUse, Z = zenvalue,theta=data$Incidence,Gh=GHI,Dh=DHI,Delta=skyBright)
    #data = dataframpass  
    factor = Perez1(dataframpass)
    Ic <- cos(d2r(data$Incidence))/cos(d2r(zenvalue))*(GHI-DHI)
    Dg <- GHI*0.5*1*(1-cos(d2r(tiltUse)))
    Dc <- factor*DHI
    gc = Ic + Dg + Dc
    gc = gc * adjustmentFactor
    if(!is.finite(gc))
    {
      print("___________________")
      print(tm)
      print(Ic)
      print(Dg)
      print(Dc)
      print("___________________")
    }
    
    if(is.finite(gc) && gc > 1800)
    {
      print("___________________")
      print(tm)
      print(Ic)
      print(Dg)
      print(Dc)
      print("___________________")
    }
    finalTm = substr(tm,1,19)
    
    TMData[globCounter] = finalTm
    GHIData[globCounter] = GHI
    DHIData[globCounter] = DHI
    GTIData[globCounter] = GTI
    TambData[globCounter] = TEMP
    TiltData[globCounter] = tiltUse
    AziData[globCounter] = aziUse
    GmodData[globCounter] = round(abs(gc),1)
    ZenData[globCounter] = round(zenvalue,1)
    Incidence[globCounter] = round(data$Incidence,1)
    skyBrightness[globCounter] = round(skyBright,3)
		latData[globCounter] = lat
		lonData[globCounter] = lon
		solarElevation[globCounter] = seuse
		if(is.finite(idxSE))
		{
			if(abs(as.numeric(seuse) + as.numeric(zenvalue)-90) > 1)
			{
				DayError[GlobalErrorCount] = day
				TmError[GlobalErrorCount] = finalTm
				FileInfoError[GlobalErrorCount] = filenameError
				ZenError[GlobalErrorCount] = round(zenvalue,2)
				SEError[GlobalErrorCount] = round(as.numeric(seuse),2)
				DiffError[GlobalErrorCount] = round(abs(as.numeric(seuse) + as.numeric(zenvalue)-90),2)
				GlobalErrorCount = GlobalErrorCount + 1
			}
		}
    if(writeFile)
    {
    print(paste('Updating',pathWriteFinal))
		if(city=='Chaksu' || city =='Nellore')
			GTIData = unlist(rep(NA,length(GTIData)))
    if(is.finite(idxSE))
		  df = data.frame(Tm=TMData,GHI=GHIData,DHI=DHIData,GTI=GTIData,Tamb=TambData,Tilt=TiltData,Azi=AziData,
                    Gmod=GmodData,Zen=ZenData,Incidence=Incidence,skyBrightness=skyBrightness,Lat = latData, Lon = lonData,SE=solarElevation,stringsAsFactors=F)
    else
      df = data.frame(Tm=TMData,GHI=GHIData,DHI=DHIData,GTI=GTIData,Tamb=TambData,Tilt=TiltData,Azi=AziData,
                      Gmod=GmodData,Zen=ZenData,Incidence=Incidence,skyBrightness=skyBrightness,Lat = latData, Lon = lonData,stringsAsFactors=F)    
    
		write.table(df,file=pathWriteFinal,row.names =F,col.names = T,sep="\t",append=F)
    resetList()
    }
    #A data.frame containing the tilt angle s, zenith angle z, incidence angle ??, GHI
    #Gh, DHI Dh and sky's brightness ???.
  }
  print(paste('Done',outer))
}
#}
df2 = data.frame(Day=DayError,Tm=TmError,File=FileInfoError,Zen=ZenError,SE=SEError,Diff=DiffError,stringsAsFactors=F)
write.table(df2,file="/home/admin/Dropbox/GIS/ZenithSEMissmatch.txt",row.names =F,col.names = T,sep="\t",append=F)
sink()

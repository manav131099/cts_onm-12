source('/home/admin/CODE/common/aggregate.R')

registerMeterList("MY-004S",c(""))
aggNameTemplate = getNameTemplate()
aggColTemplate = getColumnTemplate()

aggColTemplate[1] = 1 #Column no for date
aggColTemplate[2] = 38 #Column no for DA
aggColTemplate[3] = 32 #column for LastRead
aggColTemplate[4] = 33 #column for LastTime
aggColTemplate[5] = 30 #column for Eac-1
aggColTemplate[6] = 31 #column for Eac-2
aggColTemplate[7] = 27 #column for Yld-1
aggColTemplate[8] = 35 #column for Yld-2
aggColTemplate[9] = 28 #column for PR-1
aggColTemplate[10] = 36 #column for PR-2
aggColTemplate[11] = 4 #column for Irr
aggColTemplate[12] = "Self" # IrrSrc Value
aggColTemplate[13] = 8 #column for Tamb
aggColTemplate[14] = 20 #column for Tmod
aggColTemplate[15] = 14 #column for Hamb

registerColumnList("MY-004S","",aggNameTemplate,aggColTemplate)

import os
import glob
import pandas as pd
import numpy as np
from numpy import *
from datetime import timedelta 
import matplotlib.pyplot as plt
from matplotlib.offsetbox import AnchoredText
import statistics


from datetime import date
today = str(date.today())
print(today)
year = today[0:4]
content = []
count_pr = 0
count_energy = 0
month = today[0:7]
inv_eff_list = []

system_size = [38400, 38400, 38400, 44800, 44800, 44800, 44800, 44800, 44800, 44800, 44800, 44800, 44800,44800, 44800,]
def Inv_eff(path,i):
  df11 = pd.read_csv(path,sep='\t')
  df11.dropna(how='any') 
  df11=(df11[['Tstamp','AC_Power','DC_Power']].dropna())
  df11=df11.loc[((df11['AC_Power']>0) & (df11['DC_Power']>df11['AC_Power'])) ,] #Filters
  df11['Loading'] = df11['AC_Power']/system_size[i]
  df11['Eff'] = df11['AC_Power']/df11['DC_Power']
  df11['sumpro1'] = df11['Loading']*df11['Eff']
  sp = df11['sumpro1'].sum()
  sl = df11['Loading'].sum()
  inv_eff11 = round((sp/sl)*100,2)
  inv_eff_list.append(inv_eff11)
  
Inv_eff('/home/admin/Dropbox//FlexiMC_Data/Gen1_Data/[IN-032C]/'+year+'/'+month+'/Inverter_1/[IN-032C]-I1-'+today+'.txt',0 )
Inv_eff('/home/admin/Dropbox//FlexiMC_Data/Gen1_Data/[IN-032C]/'+year+'/'+month+'/Inverter_2/[IN-032C]-I2-'+today+'.txt',1 )
Inv_eff('/home/admin/Dropbox//FlexiMC_Data/Gen1_Data/[IN-032C]/'+year+'/'+month+'/Inverter_3/[IN-032C]-I3-'+today+'.txt',2 )
Inv_eff('/home/admin/Dropbox//FlexiMC_Data/Gen1_Data/[IN-032C]/'+year+'/'+month+'/Inverter_4/[IN-032C]-I4-'+today+'.txt',3)
Inv_eff('/home/admin/Dropbox//FlexiMC_Data/Gen1_Data/[IN-032C]/'+year+'/'+month+'/Inverter_5/[IN-032C]-I5-'+today+'.txt',4 )
Inv_eff('/home/admin/Dropbox//FlexiMC_Data/Gen1_Data/[IN-032C]/'+year+'/'+month+'/Inverter_6/[IN-032C]-I6-'+today+'.txt',5 )
Inv_eff('/home/admin/Dropbox//FlexiMC_Data/Gen1_Data/[IN-032C]/'+year+'/'+month+'/Inverter_7/[IN-032C]-I7-'+today+'.txt',6 )
Inv_eff('/home/admin/Dropbox//FlexiMC_Data/Gen1_Data/[IN-032C]/'+year+'/'+month+'/Inverter_8/[IN-032C]-I8-'+today+'.txt',7 )
Inv_eff('/home/admin/Dropbox//FlexiMC_Data/Gen1_Data/[IN-032C]/'+year+'/'+month+'/Inverter_9/[IN-032C]-I9-'+today+'.txt',8 )
Inv_eff('/home/admin/Dropbox//FlexiMC_Data/Gen1_Data/[IN-032C]/'+year+'/'+month+'/Inverter_10/[IN-032C]-I10-'+today+'.txt',9 )
Inv_eff('/home/admin/Dropbox//FlexiMC_Data/Gen1_Data/[IN-032C]/'+year+'/'+month+'/Inverter_11/[IN-032C]-I11-'+today+'.txt',10 )
Inv_eff('/home/admin/Dropbox//FlexiMC_Data/Gen1_Data/[IN-032C]/'+year+'/'+month+'/Inverter_12/[IN-032C]-I12-'+today+'.txt',11 )
Inv_eff('/home/admin/Dropbox//FlexiMC_Data/Gen1_Data/[IN-032C]/'+year+'/'+month+'/Inverter_13/[IN-032C]-I13-'+today+'.txt',12 )
Inv_eff('/home/admin/Dropbox//FlexiMC_Data/Gen1_Data/[IN-032C]/'+year+'/'+month+'/Inverter_14/[IN-032C]-I14-'+today+'.txt',13 )
Inv_eff('/home/admin/Dropbox//FlexiMC_Data/Gen1_Data/[IN-032C]/'+year+'/'+month+'/Inverter_15/[IN-032C]-I15-'+today+'.txt',14 )

x_axis = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]
LABELS = ["INV-1", "INV-2", "INV-3","INV-4","INV-5","INV-6", "INV-7", "INV-8","INV-9","INV-10","INV-11","INV-12", "INV-13", "INV-14","INV-15"]
plt.figure(figsize=(20,10))
plot = plt.bar(x_axis,inv_eff_list,align='center',color='paleturquoise',  edgecolor='black', width=0.5)
for rect in plot:
        height = rect.get_height()
        plt.text(rect.get_x() + rect.get_width()/2.0, height, '%.2f' % float(height), ha='center', va='bottom')
plt.xticks(x_axis,LABELS)       
plt.title("[IN-032C] Inverter Efficiency - "+str(today))
plt.ylabel("Inverter Efficiency [%]")

plt.rcParams.update({'font.size': 12})
axes = plt.gca()
mean = statistics.mean(inv_eff_list) 
mean = round(mean,2)
plt.axhline(y=mean, xmin=-10, xmax=100, linewidth=2, linestyle='--',color='black')
axes.set_ylim([0,105])

plt.text(14.8, 89.82, 'Mean ='+str(mean), va="bottom")
plt.savefig('/home/admin/Jason/cec intern/results/IN-032L/[IN-032C] Graph '+str(today)+' Inverter_Efficiency.pdf')
print(inv_eff_list)

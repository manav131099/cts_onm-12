source('/home/admin/CODE/common/aggregate.R')

registerMeterList("KH-002X",c("Loads","Solar"))
aggNameTemplate = getNameTemplate()
aggColTemplate = getColumnTemplate()

aggColTemplate[1] = 1 #Column no for date
aggColTemplate[2] = 6 #Column no for DA
aggColTemplate[3] = 9 #column for LastRead
aggColTemplate[4] = 8 #column for LastTime
aggColTemplate[5] = 2 #column for Eac-1
aggColTemplate[6] = 3 #column for Eac-2
aggColTemplate[7] = 17 #column for Yld-1
aggColTemplate[8] = 13 #column for Yld-2
aggColTemplate[9] = 18 #column for PR-1
aggColTemplate[10] = 14 #column for PR-2
aggColTemplate[11] = 12 #column for Irr
aggColTemplate[12] = "KH-003S" # IrrSrc Value
aggColTemplate[13] = NA #column for Tamb
aggColTemplate[14] = NA #column for Tmod
aggColTemplate[15] = NA #column for Hamb

registerColumnList("KH-002X","Loads",aggNameTemplate,aggColTemplate)

aggNameTemplate = getNameTemplate()
aggColTemplate = getColumnTemplate()

aggColTemplate[1] = 1 #Column no for date
aggColTemplate[2] = 6 #Column no for DA
aggColTemplate[3] = 9 #column for LastRead
aggColTemplate[4] = 8 #column for LastTime
aggColTemplate[5] = 2 #column for Eac-1
aggColTemplate[6] = 3 #column for Eac-2
aggColTemplate[7] = 16 #column for Yld-1
aggColTemplate[8] = 12 #column for Yld-2
aggColTemplate[9] = 17 #column for PR-1
aggColTemplate[10] = 13 #column for PR-2
aggColTemplate[11] = 11 #column for Irr
aggColTemplate[12] = "KH-003S" # IrrSrc Value
aggColTemplate[13] = NA #column for Tamb
aggColTemplate[14] = NA #column for Tmod
aggColTemplate[15] = NA #column for Hamb

registerColumnList("KH-002X","Solar",aggNameTemplate,aggColTemplate)

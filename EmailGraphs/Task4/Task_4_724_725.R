library(MASS)

#FOR SITE 724

tempList = list()
myList = list()

dailyF2Val = 0

readData = function() {
  numOfFiles = length(files)
  for (x in 1:numOfFiles) {
    #extract data from .txt file
    data = read.table(files[x], header = T, sep = "\t")
    
    #extract data from table
    dataDate = substr(files[x], (nchar(files[x]) - 13), (nchar(files[x]) - 4))
    numOfRows = length(data$Rec_ID)
    DAPercentage = round(100 * (numOfRows / 1440), digits = 1)
    SMPList = data$AvgSMP10
    GsiList = data$AvgGsi00
    SMPVal = Reduce("+",lapply(SMPList, as.numeric))/60000
    GsiVal = Reduce("+",lapply(GsiList, as.numeric))/60000
    ratioVal = SMPVal/GsiVal
    
    if (isTRUE(data$Act_E.Del_F2[length(data$Act_E.Del_F2)] != "NaN") & isTRUE(data$Act_E.Del_F2[1] != "NaN") & isTRUE(as.numeric(data$Act_E.Del_F2[length(data$Act_E.Del_F2)]) > as.numeric(data$Act_E.Del_F2[1]))) {
      F2PR = 100 * ((as.numeric(data$Act_E.Del_F2[length(data$Act_E.Del_F2)]) - as.numeric(data$Act_E.Del_F2[1]))/794.97)/GsiVal
      dailyF2Val <<- as.numeric(data$Act_E.Del_F2[length(data$Act_E.Del_F2)]) - as.numeric(data$Act_E.Del_F2[1])
    }
    else {
      F2PR = 0
      dailyF2Val <<- 0
    }
    
    tempList <<- append(tempList,c(dataDate, SMPVal, GsiVal, ratioVal, F2PR, DAPercentage))
  }
}

extractData = function() {
  for (m in 1:12) {
    if (m < 10) {
      pathprobe = paste0("/home/admin/Dropbox/Cleantechsolar/1min/[724]/",toString(y),"/",toString(y),"-","0",toString(m))
      if (file.exists(pathprobe)) {
        files <<- list.files(path = pathprobe, all.files = F, full.names = T)
        readData()
        
        #adding info to list
        myList <<- append(myList,tempList)
        tempList <<- list()
      }
      else {
        print(paste0("File ", pathprobe, " does not exist!"))
      }
    }
    else {
      pathprobe = paste0("/home/admin/Dropbox/Cleantechsolar/1min/[724]/",toString(y),"/",toString(y),"-",toString(m))
      if (file.exists(pathprobe)) {
        files <<- list.files(path = pathprobe, all.files = F, full.names = T)
        readData()
        
        #adding info to list
        myList <<- append(myList,tempList)
        tempList <<- list()
      }
      else {
        print(paste0("File ", pathprobe, " does not exist!"))
      }
    }
  }
}

#calculating number of years' worth of data is available
numOfYears = length(list.files(path = "/home/admin/Dropbox/Cleantechsolar/1min/[724]", all.files = F, full.names = T)) - 1

#loop to begin extracting data
for (y in 2018:(2018+numOfYears)) {
  extractData()
}

#obtaining satellite data
satData = read.table("/home/admin/Dropbox/GIS/Summary/Woodlands/WOOD Aggregate.txt", header = T, sep = "\t")$GHI[186:length(read.table("/home/admin/Dropbox/GIS/Summary/Woodlands/WOOD Aggregate.txt", header = T, sep = "\t")$GHI)]

#combining all data into a matrix
numDataPoints = length(myList)/6
usefulDataMatrix = matrix(nrow = (numDataPoints + 1), ncol = 7)
usefulDataMatrix[1,] = c("Date","Pyranometer","Silicon","Ratio (SMP/Gsi)","Satellite","F2 PR","Data Availability")

for(z in 1:numDataPoints) {
  usefulDataMatrix[(z+1),] = c(unlist(myList[6*(z-1) + 1]), unlist(myList[6*(z-1) + 2]), unlist(myList[6*(z-1) + 3]), unlist(myList[6*(z-1) + 4]), satData[z], unlist(myList[6*(z-1) + 5]), unlist(myList[6*(z-1) + 6]))
}

#save .txt file to the right directory
setwd("/home/admin/Jason/cec intern/results/[724]")
write.matrix(usefulDataMatrix, "allData724.txt", sep = "\t")
pdf("724_Graph.pdf",width=11,height=8)
#plot Data Availability
plotDataAvailability = function() {
  #plot time series graphs
  plot(unlist(lapply(usefulDataMatrix[2:(length(usefulDataMatrix[,1]) - 1),1],as.Date)),usefulDataMatrix[2:(length(usefulDataMatrix[,1]) - 1),7], type = "l", xaxt = "n", xlab = "", ylab = "Data Availability [%]", col = "black", lty = 1, las = 1, cex.lab = 1.3)
  
  #add titles
  title(main = paste("[724] Data Availability"), line = 2.2,cex.main = 1.5)
  title(main = paste0("From ",usefulDataMatrix[2,1], " to ", usefulDataMatrix[(nrow(usefulDataMatrix) - 1),1]), line = 0.75, cex.main = 1.2)
  
  #re-lable time-axis
  axis.Date(1, at = seq(from = as.Date(usefulDataMatrix[2,1]), to = as.Date(usefulDataMatrix[(nrow(usefulDataMatrix) - 1),1]), by = "month"), format = "%b/%y")
  
  #add names
  title(sub=paste0("Cleantech Solar","\n","O&M Team"), adj=0, line=3, font=2, cex.sub = 0.7)
  title(sub=paste0("Dhruv Baid","\n",toString(Sys.Date())), adj=1, line=3, font=2, cex.sub = 0.7)
  
  #get information about data
  averageDA = round(((Reduce("+",lapply(usefulDataMatrix[2:(length(usefulDataMatrix[,1]) - 1),7],as.numeric)))/(length(usefulDataMatrix[,1]) - 1)), digits = 1)
  lifespanSoFar = difftime(as.Date(usefulDataMatrix[(nrow(usefulDataMatrix) - 1),1]), as.Date(usefulDataMatrix[2,1]), units = c("days"))
  
  #add information about average DA to plot
  text(x = (0.45*par("usr")[2] + 0.55*par("usr")[1]), y = 70, paste0("Average data availability = ",averageDA,"%","\n","Lifetime = ",lifespanSoFar," days (",round(lifespanSoFar/365,digits = 1)," years)"), cex = 0.8, adj = 0)
}
plotDataAvailability()

#plot Daily Irradiation Bars
plotIrradiationBars = function() {
  #Plot daily irradiation bars based on Silicon sensor
  myPlot = barplot(unlist(lapply(usefulDataMatrix[33:length(usefulDataMatrix[,1]),3],as.numeric)), las = 2, mgp = c(3,0.6,0), xlab = "", ylab = expression('Daily Global Horizontal Irradiation - Silicon [kWh/m'^2*']'), cex.lab = 1.3, mgp = c(2.5,0.6,-0.2))
  
  #obtain list of dates in mmm/YYYY format
  monthList = unlist(lapply(lapply(usefulDataMatrix[33:length(usefulDataMatrix[,1]),1], as.Date), format, "%b/%Y"))
  
  #get positions of start dates of each month
  at_ticks = myPlot[which(!duplicated(monthList))]
  
  #get axis labels
  axisLabels = monthList[which(!duplicated(monthList))]
  
  #create date-axis
  axis(side = 1, at = at_ticks, labels = axisLabels, cex.lab = 0.8)
  
  #calculate average daily irradiation
  avgDailyIrradiation = Reduce("+",lapply(usefulDataMatrix[33:length(usefulDataMatrix[,1]),3],as.numeric))/(length(usefulDataMatrix[,1]) - 33 + 1)
  
  #add titles
  title(main = paste("[724] Daily Irradiation"), line = 2.2,cex.main = 1.5)
  title(main = paste0("From ",usefulDataMatrix[33,1], " to ", usefulDataMatrix[dim(usefulDataMatrix)[1],1]), line = 0.75, cex.main = 1.2)
  
  #add names
  title(sub=paste0("Cleantech Solar","\n","O&M Team"), adj=0, line=3, font=2, cex.sub = 0.7)
  title(sub=paste0("Dhruv Baid","\n",toString(Sys.Date())), adj=1, line=3, font=2, cex.sub = 0.7)
}
plotIrradiationBars()

#plot Pyranometer/Silicon ratio values
plotRatioValues = function() {
  #defining 4 lists for different colors
  blueList = list()
  lightBlueList = list()
  orangeList = list()
  brownList = list()
  
  #sort ratio values into different colors by SMP10 value
  for (i in 33:dim(usefulDataMatrix)[1]) {
    if(as.numeric(usefulDataMatrix[i,2]) < 2) {
      blueList = append(blueList, c(usefulDataMatrix[i,1],usefulDataMatrix[i,4]))
    }
    else if (as.numeric(usefulDataMatrix[i,2]) >= 2 & as.numeric(usefulDataMatrix[i,2]) < 4) {
      lightBlueList = append(lightBlueList, c(usefulDataMatrix[i,1],usefulDataMatrix[i,4]))      
    }
    else if (as.numeric(usefulDataMatrix[i,2]) >= 4 & as.numeric(usefulDataMatrix[i,2]) < 6) {
      orangeList = append(orangeList, c(usefulDataMatrix[i,1],usefulDataMatrix[i,4]))      
    }
    else {
      brownList = append(brownList, c(usefulDataMatrix[i,1],usefulDataMatrix[i,4]))      
    }
  }
  
  #defining 4 matrices
  blueMatrix = matrix(nrow = length(blueList)/2, ncol = 2)
  lightBlueMatrix = matrix(nrow = length(lightBlueList)/2, ncol = 2)
  orangeMatrix = matrix(nrow = length(orangeList)/2, ncol = 2)
  brownMatrix = matrix(nrow = length(brownList)/2, ncol = 2)
  
  #populating these matrices
  for (i in 1:nrow(blueMatrix)) {
    blueMatrix[i,] = c(unlist(blueList[2*i - 1]), unlist(blueList[2*i]))
  }
  for (i in 1:nrow(lightBlueMatrix)) {
    lightBlueMatrix[i,] = c(unlist(lightBlueList[2*i - 1]), unlist(lightBlueList[2*i]))
  }
  for (i in 1:nrow(orangeMatrix)) {
    orangeMatrix[i,] = c(unlist(orangeList[2*i - 1]), unlist(orangeList[2*i]))
  }
  for (i in 1:nrow(brownMatrix)) {
    brownMatrix[i,] = c(unlist(brownList[2*i - 1]), unlist(brownList[2*i]))
  }
  
  #Plot Pyr/Si ratio values
  plot(unlist(lapply(blueMatrix[,1],as.Date)),blueMatrix[,2], xlab = "", ylab = "Pyranometer to Silicon Sensor Readings Ratio", xaxt = "n", xlim = c(as.Date(usefulDataMatrix[33,1]), as.Date(usefulDataMatrix[dim(usefulDataMatrix)[1],1])), ylim = c(0.8,1.2), cex.lab = 1.3, mgp = c(2.5,1,0), las = 1, col = "blue", pch = 15, cex = 0.4)
  points(unlist(lapply(lightBlueMatrix[,1],as.Date)),lightBlueMatrix[,2], xlab = "", ylab = "", xaxt = "n", col = "lightblue", pch = 16, cex = 0.4)
  points(unlist(lapply(orangeMatrix[,1],as.Date)),orangeMatrix[,2], xlab = "", ylab = "", xaxt = "n", col = "orange", pch = 17, cex = 0.4)
  points(unlist(lapply(brownMatrix[,1],as.Date)),brownMatrix[,2], xlab = "", ylab = "", xaxt = "n", col = "brown", pch = 18, cex = 0.4)
  
  #Adding a line through them
  lines(spline(unlist(lapply(usefulDataMatrix[33:length(usefulDataMatrix[,1]),1],as.Date)),usefulDataMatrix[33:length(usefulDataMatrix[,1]),4]), xlab = "", ylab = "", xaxt = "n")
  
  #add date-axis label
  axis.Date(1, at = seq(from = as.Date(usefulDataMatrix[33,1]), to = as.Date(usefulDataMatrix[length(usefulDataMatrix[,1]),1]) , by = "month"), format = "%b/%Y")
  
  #Add horizontal line at ratio = 1
  abline(h = 1, lwd = 2)
  
  #Add text to explain the horizontal line
  text(x = par("usr")[2] - 0.003*(par("usr")[2] - par("usr")[1]), y = 0.99, "Ratio = 1.000", cex = 0.8, adj = 1)
  
  #calculate average ratio
  averageRatio = Reduce("+",lapply(usefulDataMatrix[33:length(usefulDataMatrix[,1]),4],as.numeric))/(length(usefulDataMatrix[,4]) - 33 + 1)
  
  #add horizontal line for average ratio
  abline(h = averageRatio, lty = 2)
  
  #add text for average ratio
  text(x = as.Date(usefulDataMatrix[103,1]), y = averageRatio + 0.018, paste0("Avg Ratio = ", round(averageRatio, digits = 3)), cex = 0.8, adj = 0)
  
  #legend
  text(x = (0.98*par("usr")[1] + 0.02*par("usr")[2]), y = 0.81, expression('Daily Irradiation [kWh/m'^2*']'), adj = 0)
  text(x = (0.74*par("usr")[1] + 0.26*par("usr")[2]), y = 0.81, "<2", adj = 0)
  text(x = (0.64*par("usr")[1] + 0.36*par("usr")[2]), y = 0.81, "2~4", adj = 0)
  text(x = (0.54*par("usr")[1] + 0.46*par("usr")[2]), y = 0.81, "4~6", adj = 0)
  text(x = (0.44*par("usr")[1] + 0.56*par("usr")[2]), y = 0.81, ">6", adj = 0)
  points((0.76*par("usr")[1] + 0.24*par("usr")[2]), 0.81, pch = 15, col = "blue")
  points((0.66*par("usr")[1] + 0.34*par("usr")[2]), 0.81, pch = 16, col = "lightblue")
  points((0.56*par("usr")[1] + 0.44*par("usr")[2]), 0.81, pch = 17, col = "orange")
  points((0.46*par("usr")[1] + 0.54*par("usr")[2]), 0.81, pch = 18, col = "brown")
  
  #add titles
  title(main = paste("[724] Pyranometer to Silicon Sensor Ratio"), line = 2.2,cex.main = 1.5)
  title(main = paste0("From ",usefulDataMatrix[33,1], " to ", usefulDataMatrix[dim(usefulDataMatrix)[1],1]), line = 0.75, cex.main = 1.2)
  
  #add names
  title(sub=paste0("Cleantech Solar","\n","O&M Team"), adj=0, line=3, font=2, cex.sub = 0.7)
  title(sub=paste0("Dhruv Baid","\n",toString(Sys.Date())), adj=1, line=3, font=2, cex.sub = 0.7)
}
plotRatioValues()

#plot satellite vs ground data
plotSatGround = function() {
  #plot points for pyranometer and silicon
  plot(usefulDataMatrix[33:length(usefulDataMatrix[,1]),3],usefulDataMatrix[33:length(usefulDataMatrix[,1]),5], xlim = c(0,7.5), ylim = c(0,7.5), xlab = expression('Daily Global Horizontal Irradiation - Ground (SMP10 or Gsi00) [kWh/m'^2*']'), ylab = expression('Daily Global Horizontal Irradiation - Satellite [kWh/m'^2*']'), col = "red", pch = 16, cex.lab = 1.3, mgp = c(2.5,1,0), las = 1)
  points(usefulDataMatrix[33:length(usefulDataMatrix[,1]),2],usefulDataMatrix[33:length(usefulDataMatrix[,1]),5], xaxt = "n", xlab = "", ylab = "", col = "blue", pch = 17)
  
  #add straight line of gradient 1 for reference
  abline(a = 0, b = 1, col = "black")
  
  #add titles
  title(main = paste("[724] Pyranometer and Silicon Sensor"), line = 2.2, cex.main = 1.5)
  title(main = paste0("From ",usefulDataMatrix[33,1], " to ", usefulDataMatrix[dim(usefulDataMatrix)[1],1]), line = 0.75, cex.main = 1.2)
  
  #legend
  legend(x = c(par("usr")[1],(0.8*par("usr")[1] + 0.2*par("usr")[2])), y = c(par("usr")[4],(0.725*par("usr")[4] + 0.275*par("usr")[3])), c("Gsi00","SMP10"), pch = c(16,17), col = c("red","blue"))
  
  #add text for number of data points
  text(x = (0.99*par("usr")[1] + 0.01*par("usr")[2]), y = (0.785*par("usr")[4] + 0.215*par("usr")[3]), paste("Number of Points = ", dim(usefulDataMatrix)[1] - 33 + 1), adj = 0)
  
  #add names
  title(sub=paste0("Cleantech Solar","\n","O&M Team"), adj=0, line=3, font=2, cex.sub = 0.7)
  title(sub=paste0("Dhruv Baid","\n",toString(Sys.Date())), adj=1, line=3, font=2, cex.sub = 0.7)
  
  #get r-squared values
  r2Gsi00 = round(summary(lm(unlist(lapply(usefulDataMatrix[33:dim(usefulDataMatrix)[1],5],as.numeric)) ~ unlist(lapply(usefulDataMatrix[33:dim(usefulDataMatrix)[1],3],as.numeric))))$r.squared, digits = 3)
  r2SMP10 = round(summary(lm(unlist(lapply(usefulDataMatrix[33:dim(usefulDataMatrix)[1],5],as.numeric)) ~ unlist(lapply(usefulDataMatrix[33:dim(usefulDataMatrix)[1],2],as.numeric))))$r.squared, digits = 3)
  
  #print r-squared values onto plot
  text(x = par("usr")[2]- 0.13*(par("usr")[2] - par("usr")[1]), y = par("usr")[3] + 0.8, bquote('R'^2 ~ " [Gsi00] =" ~ .(r2Gsi00)), adj = 0, cex = 0.8, col = "red")
  text(x = par("usr")[2]- 0.13*(par("usr")[2] - par("usr")[1]), y = par("usr")[3] + 0.4, bquote('R'^2 ~ " [SMP10] =" ~ .(r2SMP10)), adj = 0, cex = 0.8, col = "blue")
}
plotSatGround()
dev.off()
#new
#site
#new
#variables

#FOR SITE 725

tempList = list()
myList = list()

#calculating number of years' worth of data is available
numOfYears = length(list.files(path = "/home/admin/Dropbox/Cleantechsolar/1min/[725]", all.files = F, full.names = T)) - 1

dailyF3.5Val = 0

readData725 = function() {
  numOfFiles = length(files)
  for (x in 1:numOfFiles) {
    #extract data from .txt file
    data = read.table(files[x], header = T, sep = "\t")
    
    #extract data from table
    dataDate = substr(files[x], (nchar(files[x]) - 13), (nchar(files[x]) - 4))
    numOfRows = length(data$Rec_ID)
    DAPercentage = round(100 * (numOfRows / 1440), digits = 1)

    if (isTRUE(data$Act_E.Del_F3.5[length(data$Act_E.Del_F3.5)] != "NaN") & isTRUE(data$Act_E.Del_F3.5[1] != "NaN") & isTRUE(as.numeric(data$Act_E.Del_F3.5[length(data$Act_E.Del_F3.5)]) > as.numeric(data$Act_E.Del_F3.5[1]))) {
      F3.5PR = 100 * ((as.numeric(data$Act_E.Del_F3.5[length(data$Act_E.Del_F3.5)]) - as.numeric(data$Act_E.Del_F3.5[1]))/1042.8)
      dailyF3.5Val <<- as.numeric(data$Act_E.Del_F3.5[length(data$Act_E.Del_F3.5)]) - as.numeric(data$Act_E.Del_F3.5[1])
    }
    else {
      F3.5PR = 0
      dailyF3.5Val <<- 0
    }
    
    tempList <<- append(tempList,c(dataDate, DAPercentage, dailyF3.5Val, F3.5PR))
  }
}

extractData725 = function() {
  for (m in 1:12) {
    if (m < 10) {
      pathprobe = paste0("/home/admin/Dropbox/Cleantechsolar/1min/[725]/",toString(y),"/",toString(y),"-","0",toString(m))
      if (file.exists(pathprobe)) {
        files <<- list.files(path = pathprobe, all.files = F, full.names = T)
        readData725()
        
        #adding info to list
        myList <<- append(myList,tempList)
        tempList <<- list()
      }
      else {
        print(paste0("File ", pathprobe, " does not exist!"))
      }
    }
    else {
      pathprobe = paste0("/home/admin/Dropbox/Cleantechsolar/1min/[725]/",toString(y),"/",toString(y),"-",toString(m))
      if (file.exists(pathprobe)) {
        files <<- list.files(path = pathprobe, all.files = F, full.names = T)
        readData725()
        
        #adding info to list
        myList <<- append(myList,tempList)
        tempList <<- list()
      }
      else {
        print(paste0("File ", pathprobe, " does not exist!"))
      }
    }
  }
}

#loop to begin extracting data
for (y in 2018:(2018+numOfYears)) {
  extractData725()
}

#combining all data into a matrix
numDataPoints = length(myList)/4
usefulDataMatrix725 = matrix(nrow = (numDataPoints + 1), ncol = 4)
usefulDataMatrix725[1,] = c("Date","Data Availability","F3/5 Value","F3/5 PR")

for(z in 1:31) {
  usefulDataMatrix725[(z+1),] = c(unlist(myList[4*(z-1) + 1]), unlist(myList[4*(z-1) + 2]), unlist(myList[4*(z-1) + 3]), (as.numeric(unlist(myList[4*z]))/as.numeric(usefulDataMatrix[41+z,3])))
}
for(z in 32:numDataPoints) {
  usefulDataMatrix725[(z+1),] = c(unlist(myList[4*(z-1) + 1]), unlist(myList[4*(z-1) + 2]), unlist(myList[4*(z-1) + 3]), (as.numeric(unlist(myList[4*z]))/as.numeric(usefulDataMatrix[187+z,3])))
}

#save .txt file to the right directory
setwd("/home/admin/Jason/cec intern/results/[725]")
write.matrix(usefulDataMatrix725, "allData725.txt", sep = "\t")

pdf("725_Graphs.pdf",width=11,height=8)
#plot Data Availability
plotDataAvailability725 = function() {
  #plot time series graphs
  plot(unlist(lapply(usefulDataMatrix725[34:(length(usefulDataMatrix725[,1]) - 1),1],as.Date)),usefulDataMatrix725[34:(length(usefulDataMatrix725[,1]) - 1),2], type = "l", xaxt = "n", xlab = "", ylab = "Data Availability [%]", col = "black", lty = 1, las = 1, cex.lab = 1.3)
  
  #add titles
  title(main = paste("[725] Data Availability"), line = 2.2,cex.main = 1.5)
  title(main = paste0("From ",unlist(myList[4*(33-1) + 1]), " to ", unlist(myList[length(myList) - 7])), line = 0.75, cex.main = 1.2)
  
  #re-lable time-axis
  axis.Date(1, at = seq(from = as.Date(unlist(myList[4*(33-1) + 1])), to = as.Date(unlist(myList[length(myList) - 7])), by = "month"), format = "%b/%y")
  
  #add names
  title(sub=paste0("Cleantech Solar","\n","O&M Team"), adj=0, line=3, font=2, cex.sub = 0.7)
  title(sub=paste0("Dhruv Baid","\n",toString(Sys.Date())), adj=1, line=3, font=2, cex.sub = 0.7)
  
  #get information about data
  averageDA = round(((Reduce("+",lapply(usefulDataMatrix725[34:(length(usefulDataMatrix725[,1]) - 1),2],as.numeric)))/(length(usefulDataMatrix[,1]) - 34 + 1)), digits = 1)
  lifespanSoFar = difftime(as.Date(unlist(myList[length(myList) - 7])), as.Date(unlist(myList[4*(33-1) + 1])), units = c("days"))
  
  #add information about average DA to plot
  text(x = (0.45*par("usr")[2] + 0.55*par("usr")[1]), y = 70, paste0("Average data availability = ",averageDA,"%","\n","Lifetime = ",lifespanSoFar," days (",round(lifespanSoFar/365,digits = 1)," years)"), cex = 0.8, adj = 0)
}
plotDataAvailability725()

#plot PRs of both sites together
plotPRPoints = function() {
  #724
  plot(unlist(lapply(usefulDataMatrix[99:dim(usefulDataMatrix)[1],1],as.Date)),usefulDataMatrix[99:dim(usefulDataMatrix)[1],6], pch = 15, col = "green", xlab = "", ylab = "Performance Ratio [%]", xaxt = "n", xlim = c(as.Date(usefulDataMatrix[99,1]),(as.Date(usefulDataMatrix[dim(usefulDataMatrix)[1],1]) + 3)), ylim = c(0,100), cex.lab = 1.3, mgp = c(2.5,1,0), las = 1, cex = 0.7)
  #725
  points(unlist(lapply(usefulDataMatrix725[81:dim(usefulDataMatrix725)[1],1],as.Date)),usefulDataMatrix725[81:dim(usefulDataMatrix725)[1],4], pch = 16, col = "purple", cex = 0.7)
  
  #re-lable time-axis
  axis.Date(1, at = seq(from = as.Date(usefulDataMatrix[99,1]), to = as.Date(usefulDataMatrix[dim(usefulDataMatrix)[1],1]), by = "month"), format = "%b/%y")
  
  #calculate some statistics for the data
  avgF2 = mean(unlist(lapply(usefulDataMatrix[99:dim(usefulDataMatrix)[1],6],as.numeric)))
  avgF3.5 = mean(unlist(lapply(usefulDataMatrix725[81:dim(usefulDataMatrix725)[1],4], as.numeric)))
  
  #add lines corresponding to the averages
  abline(h = avgF2, col = "green", lwd = 2)
  abline(h = avgF3.5, col = "purple", lwd = 2)
  
  #add text for the averages
  if (avgF2 > avgF3.5) {
    text(x = (0.55*(par("usr")[1]) + 0.45*(par("usr")[2])), y = 50, paste0("Average (F2) = ", format(round(avgF2, digits = 1),nsmall = 1), "%"), col = "green", cex = 0.8, adj = 0)
    text(x = (0.55*(par("usr")[1]) + 0.45*(par("usr")[2])), y = 47, paste0("Average (F3/5) = ", format(round(avgF3.5, digits = 1), nsmall = 1), "%"), col = "purple", cex = 0.8, adj = 0)
  } else {
    text(x = (0.55*(par("usr")[1]) + 0.45*(par("usr")[2])), y = 47, paste0("Average (F2) = ", format(round(avgF2, digits = 1),nsmall = 1), "%"), col = "green", cex = 0.8, adj = 0)
    text(x = (0.55*(par("usr")[1]) + 0.45*(par("usr")[2])), y = 50, paste0("Average (F3/5) = ", format(round(avgF3.5, digits = 1), nsmall = 1), "%"), col = "purple", cex = 0.8, adj = 0)
  }
  
  #legend
  legend(x = c(par("usr")[1],(0.73*par("usr")[1] + 0.27*par("usr")[2])), y = c((0.2*par("usr")[4] + 0.8*par("usr")[3]),par("usr")[3]), c(paste0("F2 Sensor [",(dim(usefulDataMatrix)[1] - 99 + 1)," points]"),paste0("F3/5 Sensor [",(dim(usefulDataMatrix725)[1] - 81 + 1)," points]")), col = c("green","purple"), pch = c(15,16))
  
  #add titles
  title(main = paste("[724] and [725] Performance Ratios"), line = 2.2, cex.main = 1.5)
  title(main = paste0("From ",usefulDataMatrix[99,1], " to ", usefulDataMatrix[dim(usefulDataMatrix)[1],1]), line = 0.75, cex.main = 1.2)
  
  #add names
  title(sub=paste0("Cleantech Solar","\n","O&M Team"), adj=0, line=3, font=2, cex.sub = 0.7)
  title(sub=paste0("Dhruv Baid","\n",toString(Sys.Date())), adj=1, line=3, font=2, cex.sub = 0.7)
  
}
plotPRPoints()
dev.off()
